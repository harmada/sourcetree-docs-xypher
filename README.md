# Sourcetree Documentation
Guide for Source Tree 

----------------------------------------------------------------

### Preview
<img src="SourceTree.png"/>


----------------------------------------------------------------

## NavBar
<img src="NavBar.png"/>
Can see basic git commands:
    
### a. Commit

### b. Pull

### c. Push

### d: Fetch : Update All Local Branch

### e: Merge 
    
    i. Merge from Log
      - merge/update branch from specific commit
        *best to use for rollback and investigation

    ii. Merge Fetch 
      - merge/update branch from other branch
        *best to use this for uat merging and live branch merging

###  f: Stash: where you can get your update codes  and apply to a certain branch

      
      i. Steps
        * Go to File Status - under Sidebar
        * Stage the update
        * Stash the file/s
        * Name the stash name
        * Choose / Checkout the branch where you will apply the files - under Sidebar -branch 
        * Apply Stash - Under Sidebar
      
    
---




## Sidebar
  <img src="Sidebar.png" />

### File Status
  Check Updated, Added , Deleted file in the repo.
  Can commit and push updates to the remote repo.

### History
  Can check history of commits and push

### Search
  Can check specific commit commits



### Branches
  List Checked Out branch /Local Branch 
  * Where you can edit/modify your code
  
### Remotes
  List of remote branches
  * Where you can checkout your branch

### Stash
  List of Stash file that you can update to branch  

  





